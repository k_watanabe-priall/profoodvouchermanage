Imports Npgsql
Imports System.IO
Imports System.Text

Public Class frmLogin

#Region "Control Events"
    ''' <summary>
    ''' ログインボタン押下時
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub cmdLogin_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdLogin.Click
        Dim retInteger As Integer = 0
        Dim strMsg As String = vbNullString

        '---入力チェック
        If Me.txtUserID.Text = "" Then
            MsgBox("ユーザIDが入力されていません。" & vbCrLf & "入力してください。", vbExclamation, "ログインチェック")
            Me.txtUserID.Select()
            Exit Sub
        ElseIf Me.txtPass.Text = "" Then
            MsgBox("パスワードが入力されていません。" & vbCrLf & "入力してください。", vbExclamation, "ログインチェック")
            Me.txtUserID.Select()
            Exit Sub
        End If

        '---ログインチェック
        retInteger = fncCheckLogIn()
        '---チェック結果によるメッセージの振り分け
        Select Case retInteger
            Case RET_NOTFOUND
                strMsg = "ユーザIDが誤っています。" & vbCrLf & "ご確認ください。"
                MsgBox(strMsg, MsgBoxStyle.Exclamation, "ユーザ認証")
                Me.txtUserID.Select()
                Exit Sub
            'Case RET_WARNING
            '    strMsg = "パスワードが誤っています。" & vbCrLf & "ご確認ください。"
            '    MsgBox(strMsg, MsgBoxStyle.Exclamation, "ユーザ認証")
            '    Me.txtPass.Select()
            '    Exit Sub
            Case RET_ERROR
                strMsg = "ユーザ認証に失敗しました。" & vbCrLf & "システム管理者にご連絡ください。"
                MsgBox(strMsg, MsgBoxStyle.Critical, "ユーザ認証")
                Me.txtUserID.Select()
                Exit Sub
        End Select

        '---入力情報の保存
        My.Settings.LOGIN_USER = Me.txtUserID.Text

        If Me.chkSavePass.Checked = True Then
            My.Settings.LOGIN_PASS = Me.txtPass.Text
        Else
            My.Settings.LOGIN_PASS = ""
        End If

        My.Settings.Save()

        '---メニュー画面に遷移
        frmMenu.Show()

        '---自画面を非表示
        Me.Hide()
    End Sub

    ''' <summary>
    ''' キャンセルボタン押下時
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        Me.Close()
    End Sub

    ''' <summary>
    ''' パスワード表示のチェック状態変化時
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub chkDispPassWord_CheckedChanged(sender As Object, e As EventArgs) Handles chkDispPassWord.CheckedChanged
        If Me.chkDispPassWord.Checked = True Then
            Me.txtPass.PasswordChar = ""
        Else
            Me.txtPass.PasswordChar = "*"
        End If
    End Sub

    ''' <summary>
    ''' ユーザID カーソル取得時
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub txtUserID_GotFocus(sender As Object, e As EventArgs) Handles txtUserID.GotFocus
        Me.txtUserID.SelectionStart = 0
        Me.txtUserID.SelectionLength = Me.txtUserID.Text.Length
    End Sub

    ''' <summary>
    ''' パスワードカード取得時
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub txtPass_GotFocus(sender As Object, e As EventArgs) Handles txtPass.GotFocus
        Me.txtPass.SelectionStart = 0
        Me.txtPass.SelectionLength = Me.txtPass.Text.Length
    End Sub

    Private Sub frmLogin_Load(sender As Object, e As EventArgs) Handles Me.Load
        Me.txtUserID.Text = vbNullString
        Me.txtPass.Text = vbNullString

        Me.txtUserID.Text = My.Settings.LOGIN_USER
        Me.txtPass.Text = My.Settings.LOGIN_PASS
        Me.chkSavePass.Checked = My.Settings.SAVE_PASS
    End Sub

    Private Sub frmLogin_Closed(sender As Object, e As EventArgs) Handles Me.Closed
        If Me.chkSavePass.Checked = True Then
            My.Settings.LOGIN_PASS = Me.txtPass.Text.Trim
        Else
            My.Settings.LOGIN_PASS = ""
        End If
        My.Settings.LOGIN_USER = Me.txtUserID.Text.Trim
        My.Settings.SAVE_PASS = Me.chkSavePass.Checked

        My.Settings.Save()
    End Sub

    ''' <summary>
    ''' キーを入力されたとき
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub txtUserID_KeyDown(sender As Object, e As KeyEventArgs) Handles txtUserID.KeyDown
        If e.KeyCode = Keys.Enter Then
            If Me.txtPass.Text.Trim = "" Then
                Me.txtPass.Select()
            Else
                Me.cmdLogin.PerformClick()
            End If
        End If
    End Sub

    ''' <summary>
    ''' キーを入力されたとき
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>

    Private Sub txtPass_KeyDown(sender As Object, e As KeyEventArgs) Handles txtPass.KeyDown
        If e.KeyCode = Keys.Enter Then
            If Me.txtUserID.Text.Trim = "" Then
                Me.txtUserID.Select()
            Else
                Me.cmdLogin.PerformClick()
            End If
        End If

    End Sub

#End Region

#Region "Sub Routine"
    ''' <summary>
    ''' ログイン認証
    ''' </summary>
    ''' <returns>Integer・・・0=正常、-1=未存在</returns>
    Private Function fncCheckLogIn() As Integer
        Dim strMethod As String = System.Reflection.MethodBase.GetCurrentMethod.Name
        Dim strSQL As New System.Text.StringBuilder
        Dim strPass As String = vbNullString
        Dim ds As New DataSet
        Dim dt As New d_prim_manurtyDataSet.EiyouUserDataTable
        Dim objData As New d_prim_manurtyDataSetTableAdapters.EiyouUserTableAdapter
        Dim strID As String = vbNullString
        Dim objAdmin As New d_prim_manurtyDataSetTableAdapters.UsersTableAdapter
        Dim dtAdmin As New d_prim_manurtyDataSet.UsersDataTable


        Try

            objData.FillByLogIn(dt, Me.txtUserID.Text.Trim, Me.txtPass.Text.Trim)
            If dt.Rows.Count = 0 Then
                If Me.txtUserID.Text.Trim <> "admin" Then
                    Return RET_NOTFOUND
                End If
                objAdmin.FillByLogIn(dtAdmin, Me.txtUserID.Text.Trim)
                If dtAdmin.Rows.Count = 0 Then
                    Return RET_NOTFOUND
                Else
                    If Me.txtPass.Text.Trim <> dtAdmin.Rows(0).Item("password") Then
                        Return RET_NOTFOUND
                    End If
                End If
            End If

            Return RET_NORMAL

        Catch ex As Exception
            Call subOutLog(RET_ERROR, ex.Message, strMethod)

            Return RET_ERROR
        Finally
            Call fncDBDisConnect()
        End Try
    End Function

#End Region

End Class
