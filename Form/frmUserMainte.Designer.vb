﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmUserMainte
    Inherits System.Windows.Forms.Form

    'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows フォーム デザイナーで必要です。
    Private components As System.ComponentModel.IContainer

    'メモ: 以下のプロシージャは Windows フォーム デザイナーで必要です。
    'Windows フォーム デザイナーを使用して変更できます。  
    'コード エディターを使って変更しないでください。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.lstUser = New System.Windows.Forms.ListBox()
        Me.EiyouUserBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.D_prim_manurtyDataSet = New proFoodVoucherManage.d_prim_manurtyDataSet()
        Me.lstFoodUser = New System.Windows.Forms.ListBox()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.cmdLeft2Right = New System.Windows.Forms.Button()
        Me.cmdRight2Left = New System.Windows.Forms.Button()
        Me.TableLayoutPanel3 = New System.Windows.Forms.TableLayoutPanel()
        Me.cmdClose = New System.Windows.Forms.Button()
        Me.EiyouUserTableAdapter = New proFoodVoucherManage.d_prim_manurtyDataSetTableAdapters.EiyouUserTableAdapter()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        CType(Me.EiyouUserBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.D_prim_manurtyDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.TableLayoutPanel3.SuspendLayout()
        Me.SuspendLayout()
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.TableLayoutPanel1)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(963, 600)
        Me.FlowLayoutPanel1.TabIndex = 0
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 3
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70.79797!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 29.20204!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 394.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.lstUser, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.lstFoodUser, 2, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.TableLayoutPanel2, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.TableLayoutPanel3, 2, 1)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(3, 3)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 2
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 87.43719!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.56281!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(960, 597)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'lstUser
        '
        Me.lstUser.DataSource = Me.EiyouUserBindingSource
        Me.lstUser.DisplayMember = "name"
        Me.lstUser.FormattingEnabled = True
        Me.lstUser.ItemHeight = 16
        Me.lstUser.Location = New System.Drawing.Point(3, 3)
        Me.lstUser.Name = "lstUser"
        Me.lstUser.Size = New System.Drawing.Size(394, 516)
        Me.lstUser.TabIndex = 0
        Me.lstUser.ValueMember = "id"
        '
        'EiyouUserBindingSource
        '
        Me.EiyouUserBindingSource.DataMember = "EiyouUser"
        Me.EiyouUserBindingSource.DataSource = Me.D_prim_manurtyDataSet
        '
        'D_prim_manurtyDataSet
        '
        Me.D_prim_manurtyDataSet.DataSetName = "d_prim_manurtyDataSet"
        Me.D_prim_manurtyDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'lstFoodUser
        '
        Me.lstFoodUser.FormattingEnabled = True
        Me.lstFoodUser.ItemHeight = 16
        Me.lstFoodUser.Location = New System.Drawing.Point(568, 3)
        Me.lstFoodUser.Name = "lstFoodUser"
        Me.lstFoodUser.Size = New System.Drawing.Size(389, 516)
        Me.lstFoodUser.TabIndex = 1
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 1
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.cmdLeft2Right, 0, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.cmdRight2Left, 0, 3)
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(403, 3)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 5
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 88.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 175.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 77.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(159, 516)
        Me.TableLayoutPanel2.TabIndex = 2
        '
        'cmdLeft2Right
        '
        Me.cmdLeft2Right.Font = New System.Drawing.Font("HG丸ｺﾞｼｯｸM-PRO", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.cmdLeft2Right.Location = New System.Drawing.Point(3, 91)
        Me.cmdLeft2Right.Name = "cmdLeft2Right"
        Me.cmdLeft2Right.Size = New System.Drawing.Size(153, 82)
        Me.cmdLeft2Right.TabIndex = 0
        Me.cmdLeft2Right.Text = "→"
        Me.cmdLeft2Right.UseVisualStyleBackColor = True
        '
        'cmdRight2Left
        '
        Me.cmdRight2Left.Font = New System.Drawing.Font("HG丸ｺﾞｼｯｸM-PRO", 24.0!)
        Me.cmdRight2Left.Location = New System.Drawing.Point(3, 354)
        Me.cmdRight2Left.Name = "cmdRight2Left"
        Me.cmdRight2Left.Size = New System.Drawing.Size(153, 82)
        Me.cmdRight2Left.TabIndex = 1
        Me.cmdRight2Left.Text = "←"
        Me.cmdRight2Left.UseVisualStyleBackColor = True
        '
        'TableLayoutPanel3
        '
        Me.TableLayoutPanel3.ColumnCount = 2
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel3.Controls.Add(Me.cmdClose, 1, 0)
        Me.TableLayoutPanel3.Location = New System.Drawing.Point(568, 525)
        Me.TableLayoutPanel3.Name = "TableLayoutPanel3"
        Me.TableLayoutPanel3.RowCount = 1
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel3.Size = New System.Drawing.Size(389, 69)
        Me.TableLayoutPanel3.TabIndex = 3
        '
        'cmdClose
        '
        Me.cmdClose.Location = New System.Drawing.Point(197, 3)
        Me.cmdClose.Name = "cmdClose"
        Me.cmdClose.Size = New System.Drawing.Size(189, 63)
        Me.cmdClose.TabIndex = 0
        Me.cmdClose.Text = "閉じる"
        Me.cmdClose.UseVisualStyleBackColor = True
        '
        'EiyouUserTableAdapter
        '
        Me.EiyouUserTableAdapter.ClearBeforeFill = True
        '
        'frmUserMainte
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(11.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.ClientSize = New System.Drawing.Size(963, 600)
        Me.ControlBox = False
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.Font = New System.Drawing.Font("HG丸ｺﾞｼｯｸM-PRO", 12.0!)
        Me.Margin = New System.Windows.Forms.Padding(6, 4, 6, 4)
        Me.Name = "frmUserMainte"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "ユーザメンテナンス"
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        CType(Me.EiyouUserBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.D_prim_manurtyDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.TableLayoutPanel3.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents FlowLayoutPanel1 As FlowLayoutPanel
    Friend WithEvents TableLayoutPanel1 As TableLayoutPanel
    Friend WithEvents lstUser As ListBox
    Friend WithEvents lstFoodUser As ListBox
    Friend WithEvents TableLayoutPanel2 As TableLayoutPanel
    Friend WithEvents cmdLeft2Right As Button
    Friend WithEvents cmdRight2Left As Button
    Friend WithEvents TableLayoutPanel3 As TableLayoutPanel
    Friend WithEvents cmdClose As Button
    Friend WithEvents D_prim_manurtyDataSet As d_prim_manurtyDataSet
    Friend WithEvents EiyouUserBindingSource As BindingSource
    Friend WithEvents EiyouUserTableAdapter As d_prim_manurtyDataSetTableAdapters.EiyouUserTableAdapter
End Class
