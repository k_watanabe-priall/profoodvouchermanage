﻿Imports System.ComponentModel

Public Class frmMenuMainte
#Region "FormEvents"
    ''' <summary>
    ''' FormLoad
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub frmMenuMainte_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim strMethod As String = System.Reflection.MethodBase.GetCurrentMethod.Name

        '---一覧の初期化
        Me.dgvList.Rows.Clear()

        '---入力エリアの初期化
        Call subInputAreaInit()

        '---メニューデータの初期表示
        If fncGetMenuData() = RET_ERROR Then
            Call subOutLog(RET_ERROR, "メニューデータの初期表示に失敗しました。", strMethod)
        End If
    End Sub

    ''' <summary>
    ''' フォームが閉じられるとき
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub frmMenuMainte_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
        frmMenu.Visible = True
    End Sub

#End Region

#Region "Control Events"
    ''' <summary>
    ''' 入力エリアの初期化
    ''' </summary>
    Private Sub subInputAreaInit()
        Me.txtCardReader.Text = vbNullString
        Me.txtMenu.Text = vbNullString
        Me.txtMenuContent.Text = vbNullString
        Me.txtPrice.Text = vbNullString
        Me.txtMenuAno.Text = vbNullString
    End Sub

    ''' <summary>
    ''' 新規追加ボタン押下
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub cmdAdd_Click(sender As Object, e As EventArgs) Handles cmdAdd.Click
        Dim strMethod As String = System.Reflection.MethodBase.GetCurrentMethod.Name

        '必須入力チェック
        If Me.txtCardReader.Text = "" Then
            Call subOutMessage(RET_WARNING, "カードリーダ名が入力されていません。" & vbCrLf & "入力して下さい。", "入力チェック")
            Me.txtCardReader.Select()
            Exit Sub
        End If
        If Me.txtMenu.Text = "" Then
            Call subOutMessage(RET_WARNING, "メニューが入力されていません。" & vbCrLf & "入力して下さい。", "入力チェック")
            Me.txtMenu.Select()
            Exit Sub
        End If
        If Me.txtPrice.Text = "" Then
            Call subOutMessage(RET_WARNING, "価格が入力されていません。" & vbCrLf & "入力して下さい。", "入力チェック")
            Me.txtPrice.Select()
            Exit Sub
        End If

        '---データ登録
        If fncInsMstMenu() = RET_NORMAL Then
            Call subOutMessage(RET_NORMAL, "メニュデータを登録しました。", strMethod)
        End If

        '---一覧再表示
        Call fncGetMenuData()
    End Sub

    ''' <summary>
    ''' メニュー一覧選択時
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub dgvList_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvList.CellClick
        Me.txtCardReader.Text = Me.dgvList.Rows(e.RowIndex).Cells("COL_DEVICE").Value           'カードリーダ名
        Me.txtMenu.Text = Me.dgvList.Rows(e.RowIndex).Cells("COL_MENU").Value                   'メニュー
        Me.txtMenuContent.Text = Me.dgvList.Rows(e.RowIndex).Cells("COL_MENU_CONTENT").Value    'メニュー内容
        Me.txtPrice.Text = Me.dgvList.Rows(e.RowIndex).Cells("COL_PRICE").Value                 '価格
        Me.txtMenuAno.Text = Me.dgvList.Rows(e.RowIndex).Cells("COL_MENYU_ANO").Value           'メニュー登録番号
    End Sub

    ''' <summary>
    ''' 閉じるボタン押下
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub cmdClose_Click(sender As Object, e As EventArgs) Handles cmdClose.Click
        Me.Close()

        frmMenu.Visible = True
    End Sub

    ''' <summary>
    ''' 更新ボタン押下
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub cmdUpDate_Click(sender As Object, e As EventArgs) Handles cmdUpDate.Click
        Dim strMethod As String = System.Reflection.MethodBase.GetCurrentMethod.Name

        '必須入力チェック
        If Me.txtCardReader.Text = "" Then
            Call subOutMessage(RET_WARNING, "カードリーダ名が入力されていません。" & vbCrLf & "入力して下さい。", "入力チェック")
            Me.txtCardReader.Select()
            Exit Sub
        End If
        If Me.txtMenu.Text = "" Then
            Call subOutMessage(RET_WARNING, "メニューが入力されていません。" & vbCrLf & "入力して下さい。", "入力チェック")
            Me.txtMenu.Select()
            Exit Sub
        End If
        If Me.txtPrice.Text = "" Then
            Call subOutMessage(RET_WARNING, "価格が入力されていません。" & vbCrLf & "入力して下さい。", "入力チェック")
            Me.txtPrice.Select()
            Exit Sub
        End If

        '---データ更新
        If fncUpdMenu() = RET_NORMAL Then
            Call subOutMessage(RET_NORMAL, "メニューデータを更新しました。", strMethod)
        End If

        '---一覧再表示
        Call fncGetMenuData()
    End Sub


#End Region

#Region "Sub Routine"

    ''' <summary>
    ''' メニューマスタの取得/一覧表示
    ''' </summary>
    ''' <returns></returns>
    Private Function fncGetMenuData() As Integer
        Dim strMethod As String = System.Reflection.MethodBase.GetCurrentMethod.Name
        Dim strSQL As New System.Text.StringBuilder
        Dim ds As New DataSet
        Dim intCounter As Integer = 0

        Try
            '---DB接続
            Call fncDBConnect()

            '---SQL文生成
            strSQL.Clear()
            strSQL.AppendLine("SELECT ")
            strSQL.AppendLine("MenuAno,")
            strSQL.AppendLine("TerminalID,")
            strSQL.AppendLine("MenuName,")
            strSQL.AppendLine("MenuContent,")
            strSQL.AppendLine("Price")
            strSQL.AppendLine("FROM MstMenu")
            strSQL.AppendLine("WHERE DelFlg = 'False'")

            '---SQL文発行
            ds = fncAdptSQL(objConnect_LocalDB, strSQL.ToString)

            '---リストの初期化
            Me.dgvList.Rows.Clear()

            If ds.Tables.Count = 0 Or ds.Tables(0).Rows.Count = 0 Then
                Return RET_NOTFOUND
            End If

            '---リスト表示
            For intCounter = 0 To ds.Tables(0).Rows.Count - 1
                Me.dgvList.Rows.Add()

                With Me.dgvList.Rows(intCounter)
                    .Cells("COL_DEVICE").Value = ds.Tables(0).Rows(intCounter).Item("TerminalID")
                    .Cells("COL_MENU").Value = ds.Tables(0).Rows(intCounter).Item("MenuName")
                    .Cells("COL_MENU_CONTENT").Value = ds.Tables(0).Rows(intCounter).Item("MenuContent")
                    .Cells("COL_PRICE").Value = ds.Tables(0).Rows(intCounter).Item("Price")
                    .Cells("COL_MENYU_ANO").Value = ds.Tables(0).Rows(intCounter).Item("MenuAno")
                End With
            Next intCounter

            Return RET_NORMAL
        Catch ex As Exception
            Call subOutLog(RET_ERROR, ex.Message, strMethod)

            Return RET_ERROR
        Finally
            Call fncDBDisConnect()
        End Try
    End Function

    ''' <summary>
    ''' データ登録
    ''' </summary>
    Private Function fncInsMstMenu() As Integer
        Dim strMethod As String = System.Reflection.MethodBase.GetCurrentMethod.Name
        Dim strSQL As New System.Text.StringBuilder

        Try
            '---DB接続
            Call fncDBConnect()

            '---SQL文生成
            strSQL.Clear()
            strSQL.AppendLine("INSERT INTO MstMenu")
            strSQL.AppendLine("(")
            strSQL.AppendLine("TerminalID,")
            strSQL.AppendLine("MenuName,")
            strSQL.AppendLine("MenuContent,")
            strSQL.AppendLine("Price,")
            strSQL.AppendLine("InsDate,")
            strSQL.AppendLine("InsTime")
            strSQL.AppendLine(")")
            strSQL.AppendLine("VALUES")
            strSQL.AppendLine("(")
            strSQL.AppendLine("'" & Me.txtCardReader.Text.Trim & "',")
            strSQL.AppendLine("'" & Me.txtMenu.Text.Trim & "',")
            strSQL.AppendLine("'" & Me.txtMenuContent.Text.Trim & "',")
            strSQL.AppendLine("'" & Me.txtPrice.Text.Trim & "',")
            strSQL.AppendLine("'" & DateTime.Now.ToString("yyyyMMdd") & "',")
            strSQL.AppendLine("'" & DateTime.Now.ToString("HHmmss") & "'")
            strSQL.AppendLine(")")

            '---SQL文発行
            Call fncExecuteNonQuery(objConnect_LocalDB, strSQL.ToString)

            Return RET_NORMAL
        Catch ex As Exception
            Call subOutMessage(RET_ERROR, ex.Message, strMethod)

            Return RET_ERROR
        Finally
            Call fncDBDisConnect()
        End Try
    End Function

    ''' <summary>
    ''' メニューデータ更新処理
    ''' </summary>
    ''' <returns></returns>
    Private Function fncUpdMenu() As Integer
        Dim strMethod As String = System.Reflection.MethodBase.GetCurrentMethod.Name
        Dim strSQL As New System.Text.StringBuilder

        Try
            '---DB接続
            Call fncDBConnect()

            '---SQL文生成
            strSQL.Clear()
            strSQL.AppendLine("UPDATE MstMenu")
            strSQL.AppendLine("SET TerminalID = '" & Me.txtCardReader.Text.Trim & "',")
            strSQL.AppendLine("MenuName = '" & Me.txtMenu.Text.Trim & "',")
            strSQL.AppendLine("MenuContent = '" & Me.txtMenuContent.Text.Trim & "',")
            strSQL.AppendLine("Price = '" & Me.txtPrice.Text.Trim & "',")
            strSQL.AppendLine("UpdDate = '" & System.DateTime.Now.ToString("yyyyMMdd") & "',")
            strSQL.AppendLine("UpdTime = '" & System.DateTime.Now.ToString("HHmmss") & "'")
            strSQL.AppendLine("WHERE MenuAno = " & Me.txtMenuAno.Text.Trim & "")

            '---SQL文発行
            Call fncExecuteNonQuery(objConnect_LocalDB, strSQL.ToString)

            Return RET_NORMAL
        Catch ex As Exception
            Call subOutMessage(RET_ERROR, ex.Message, strMethod)

            Return RET_ERROR
        Finally
            Call fncDBDisConnect()
        End Try
    End Function

#End Region

End Class