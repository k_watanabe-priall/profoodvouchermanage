﻿Public Class frmMenu
    Private Sub cmdHistory_Click(sender As Object, e As EventArgs) Handles cmdHistory.Click
        frmHistory.Show()

        Me.Visible = False
    End Sub

    Private Sub cmdMenuMainte_Click(sender As Object, e As EventArgs) Handles cmdMenuMainte.Click
        frmMenuMainte.Show()

        Me.Visible = False
    End Sub

    Private Sub cmdUserMainte_Click(sender As Object, e As EventArgs) Handles cmdUserMainte.Click
        frmUserMainte.Show()

        Me.Visible = False
    End Sub

    Private Sub cmdBack_Click(sender As Object, e As EventArgs) Handles cmdBack.Click
        Me.Close()

        frmLogin.Visible = True
    End Sub

    Private Sub cmdAggregate_Click(sender As Object, e As EventArgs) Handles cmdAggregate.Click
        Me.Close()

        frmAggregate.Visible = True
    End Sub

    Private Sub frmMenu_Load(sender As Object, e As EventArgs) Handles Me.Load

    End Sub
End Class
