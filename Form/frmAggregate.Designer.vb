﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAggregate
    Inherits System.Windows.Forms.Form

    'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows フォーム デザイナーで必要です。
    Private components As System.ComponentModel.IContainer

    'メモ: 以下のプロシージャは Windows フォーム デザイナーで必要です。
    'Windows フォーム デザイナーを使用して変更できます。  
    'コード エディターを使って変更しないでください。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.dgvList = New System.Windows.Forms.DataGridView()
        Me.COL_PURCHASE_DATE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.COL_GROUP_CD = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.COL_SECTION_CD = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.COL_USER_ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.COL_USER_NAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.COL_USER_KANA = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.COL_MENU_A = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.COL_MENU_B = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.COL_MENU_C = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.COL_ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.COL_CID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.COL_SUM = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtEmpCode = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtKanaName = New System.Windows.Forms.TextBox()
        Me.nupYear = New System.Windows.Forms.NumericUpDown()
        Me.optPurchase2 = New System.Windows.Forms.RadioButton()
        Me.optPurchase1 = New System.Windows.Forms.RadioButton()
        Me.nupYearFrom = New System.Windows.Forms.NumericUpDown()
        Me.nupMont = New System.Windows.Forms.NumericUpDown()
        Me.nupMontFrom = New System.Windows.Forms.NumericUpDown()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cmdClear = New System.Windows.Forms.Button()
        Me.cmdSearch = New System.Windows.Forms.Button()
        Me.nupYearTo = New System.Windows.Forms.NumericUpDown()
        Me.nupMontTo = New System.Windows.Forms.NumericUpDown()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.cmdClose = New System.Windows.Forms.Button()
        Me.cmdSaveCSV = New System.Windows.Forms.Button()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.optDIV2 = New System.Windows.Forms.RadioButton()
        Me.optDIV1 = New System.Windows.Forms.RadioButton()
        Me.sfdCsvFile = New System.Windows.Forms.SaveFileDialog()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.optDayMon2 = New System.Windows.Forms.RadioButton()
        Me.optDayMon1 = New System.Windows.Forms.RadioButton()
        Me.lblRowCount = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.GroupBox2.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.dgvList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        CType(Me.nupYear, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nupYearFrom, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nupMont, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nupMontFrom, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nupYearTo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nupMontTo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox2
        '
        Me.GroupBox2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox2.Controls.Add(Me.Panel2)
        Me.GroupBox2.Location = New System.Drawing.Point(8, 192)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(1249, 688)
        Me.GroupBox2.TabIndex = 3
        Me.GroupBox2.TabStop = False
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.dgvList)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(3, 19)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(1243, 666)
        Me.Panel2.TabIndex = 0
        '
        'dgvList
        '
        Me.dgvList.AllowUserToAddRows = False
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("HG丸ｺﾞｼｯｸM-PRO", 12.0!)
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvList.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvList.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.COL_PURCHASE_DATE, Me.COL_GROUP_CD, Me.COL_SECTION_CD, Me.COL_USER_ID, Me.COL_USER_NAME, Me.COL_USER_KANA, Me.COL_MENU_A, Me.COL_MENU_B, Me.COL_MENU_C, Me.COL_ID, Me.COL_CID, Me.COL_SUM})
        Me.dgvList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvList.Location = New System.Drawing.Point(0, 0)
        Me.dgvList.Name = "dgvList"
        Me.dgvList.RowTemplate.Height = 21
        Me.dgvList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvList.Size = New System.Drawing.Size(1243, 666)
        Me.dgvList.TabIndex = 0
        '
        'COL_PURCHASE_DATE
        '
        Me.COL_PURCHASE_DATE.HeaderText = "購入日"
        Me.COL_PURCHASE_DATE.Name = "COL_PURCHASE_DATE"
        Me.COL_PURCHASE_DATE.Width = 180
        '
        'COL_GROUP_CD
        '
        Me.COL_GROUP_CD.HeaderText = "部コード"
        Me.COL_GROUP_CD.Name = "COL_GROUP_CD"
        '
        'COL_SECTION_CD
        '
        Me.COL_SECTION_CD.HeaderText = "課コード"
        Me.COL_SECTION_CD.Name = "COL_SECTION_CD"
        '
        'COL_USER_ID
        '
        Me.COL_USER_ID.HeaderText = "職員番号"
        Me.COL_USER_ID.Name = "COL_USER_ID"
        Me.COL_USER_ID.Width = 150
        '
        'COL_USER_NAME
        '
        Me.COL_USER_NAME.HeaderText = "職員氏名"
        Me.COL_USER_NAME.Name = "COL_USER_NAME"
        Me.COL_USER_NAME.Width = 200
        '
        'COL_USER_KANA
        '
        Me.COL_USER_KANA.HeaderText = "カナ氏名"
        Me.COL_USER_KANA.Name = "COL_USER_KANA"
        Me.COL_USER_KANA.Width = 200
        '
        'COL_MENU_A
        '
        Me.COL_MENU_A.HeaderText = "A"
        Me.COL_MENU_A.Name = "COL_MENU_A"
        Me.COL_MENU_A.Visible = False
        '
        'COL_MENU_B
        '
        Me.COL_MENU_B.HeaderText = "B"
        Me.COL_MENU_B.Name = "COL_MENU_B"
        Me.COL_MENU_B.Visible = False
        '
        'COL_MENU_C
        '
        Me.COL_MENU_C.HeaderText = "C"
        Me.COL_MENU_C.Name = "COL_MENU_C"
        Me.COL_MENU_C.Visible = False
        '
        'COL_ID
        '
        Me.COL_ID.HeaderText = "ユーザID"
        Me.COL_ID.Name = "COL_ID"
        Me.COL_ID.Visible = False
        '
        'COL_CID
        '
        Me.COL_CID.HeaderText = "カードUID"
        Me.COL_CID.Name = "COL_CID"
        Me.COL_CID.Visible = False
        '
        'COL_SUM
        '
        Me.COL_SUM.HeaderText = "合計金額"
        Me.COL_SUM.Name = "COL_SUM"
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.Panel1)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 79)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(1249, 110)
        Me.GroupBox1.TabIndex = 2
        Me.GroupBox1.TabStop = False
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.TableLayoutPanel1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(3, 19)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1243, 88)
        Me.Panel1.TabIndex = 0
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel1.ColumnCount = 14
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 42.03297!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 57.96703!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 87.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 83.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 54.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 87.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 51.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 81.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 106.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 156.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 28.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 161.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.Label1, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.txtEmpCode, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Label2, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.txtKanaName, 1, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.nupYear, 2, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.optPurchase2, 4, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.optPurchase1, 2, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.nupYearFrom, 4, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.nupMont, 3, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.nupMontFrom, 5, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Label3, 6, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.cmdClear, 13, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.cmdSearch, 11, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.nupYearTo, 7, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.nupMontTo, 8, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.lblRowCount, 9, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Label4, 10, 1)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(3, 3)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 3
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 41.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(1237, 97)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(3, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(72, 16)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "職員番号"
        '
        'txtEmpCode
        '
        Me.txtEmpCode.Location = New System.Drawing.Point(3, 31)
        Me.txtEmpCode.Name = "txtEmpCode"
        Me.txtEmpCode.Size = New System.Drawing.Size(104, 23)
        Me.txtEmpCode.TabIndex = 4
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(113, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(72, 16)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "カナ氏名"
        '
        'txtKanaName
        '
        Me.txtKanaName.Location = New System.Drawing.Point(113, 31)
        Me.txtKanaName.Name = "txtKanaName"
        Me.txtKanaName.Size = New System.Drawing.Size(146, 23)
        Me.txtKanaName.TabIndex = 5
        '
        'nupYear
        '
        Me.nupYear.Location = New System.Drawing.Point(265, 31)
        Me.nupYear.Maximum = New Decimal(New Integer() {2030, 0, 0, 0})
        Me.nupYear.Minimum = New Decimal(New Integer() {2020, 0, 0, 0})
        Me.nupYear.Name = "nupYear"
        Me.nupYear.Size = New System.Drawing.Size(76, 23)
        Me.nupYear.TabIndex = 6
        Me.nupYear.Value = New Decimal(New Integer() {2020, 0, 0, 0})
        '
        'optPurchase2
        '
        Me.optPurchase2.AutoSize = True
        Me.optPurchase2.Location = New System.Drawing.Point(407, 3)
        Me.optPurchase2.Name = "optPurchase2"
        Me.optPurchase2.Size = New System.Drawing.Size(74, 20)
        Me.optPurchase2.TabIndex = 3
        Me.optPurchase2.TabStop = True
        Me.optPurchase2.Text = "月範囲"
        Me.optPurchase2.UseVisualStyleBackColor = True
        '
        'optPurchase1
        '
        Me.optPurchase1.AutoSize = True
        Me.optPurchase1.Checked = True
        Me.optPurchase1.Location = New System.Drawing.Point(265, 3)
        Me.optPurchase1.Name = "optPurchase1"
        Me.optPurchase1.Size = New System.Drawing.Size(74, 20)
        Me.optPurchase1.TabIndex = 2
        Me.optPurchase1.TabStop = True
        Me.optPurchase1.Text = "月個別"
        Me.optPurchase1.UseVisualStyleBackColor = True
        '
        'nupYearFrom
        '
        Me.nupYearFrom.Location = New System.Drawing.Point(407, 31)
        Me.nupYearFrom.Maximum = New Decimal(New Integer() {2030, 0, 0, 0})
        Me.nupYearFrom.Minimum = New Decimal(New Integer() {2020, 0, 0, 0})
        Me.nupYearFrom.Name = "nupYearFrom"
        Me.nupYearFrom.Size = New System.Drawing.Size(76, 23)
        Me.nupYearFrom.TabIndex = 8
        Me.nupYearFrom.Value = New Decimal(New Integer() {2020, 0, 0, 0})
        '
        'nupMont
        '
        Me.nupMont.Location = New System.Drawing.Point(352, 31)
        Me.nupMont.Maximum = New Decimal(New Integer() {12, 0, 0, 0})
        Me.nupMont.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nupMont.Name = "nupMont"
        Me.nupMont.Size = New System.Drawing.Size(49, 23)
        Me.nupMont.TabIndex = 7
        Me.nupMont.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'nupMontFrom
        '
        Me.nupMontFrom.Location = New System.Drawing.Point(490, 31)
        Me.nupMontFrom.Maximum = New Decimal(New Integer() {12, 0, 0, 0})
        Me.nupMontFrom.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nupMontFrom.Name = "nupMontFrom"
        Me.nupMontFrom.Size = New System.Drawing.Size(48, 23)
        Me.nupMontFrom.TabIndex = 9
        Me.nupMontFrom.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'Label3
        '
        Me.Label3.Font = New System.Drawing.Font("HG丸ｺﾞｼｯｸM-PRO", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label3.Location = New System.Drawing.Point(544, 28)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(19, 11)
        Me.Label3.TabIndex = 10
        Me.Label3.Text = "~"
        '
        'cmdClear
        '
        Me.cmdClear.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdClear.Location = New System.Drawing.Point(1078, 31)
        Me.cmdClear.Name = "cmdClear"
        Me.cmdClear.Size = New System.Drawing.Size(128, 35)
        Me.cmdClear.TabIndex = 14
        Me.cmdClear.Text = "クリア"
        Me.cmdClear.UseVisualStyleBackColor = True
        '
        'cmdSearch
        '
        Me.cmdSearch.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdSearch.Location = New System.Drawing.Point(894, 31)
        Me.cmdSearch.Name = "cmdSearch"
        Me.cmdSearch.Size = New System.Drawing.Size(142, 35)
        Me.cmdSearch.TabIndex = 13
        Me.cmdSearch.Text = "検　索"
        Me.cmdSearch.UseVisualStyleBackColor = True
        '
        'nupYearTo
        '
        Me.nupYearTo.Location = New System.Drawing.Point(569, 31)
        Me.nupYearTo.Maximum = New Decimal(New Integer() {2030, 0, 0, 0})
        Me.nupYearTo.Minimum = New Decimal(New Integer() {2020, 0, 0, 0})
        Me.nupYearTo.Name = "nupYearTo"
        Me.nupYearTo.Size = New System.Drawing.Size(76, 23)
        Me.nupYearTo.TabIndex = 11
        Me.nupYearTo.Value = New Decimal(New Integer() {2020, 0, 0, 0})
        '
        'nupMontTo
        '
        Me.nupMontTo.Location = New System.Drawing.Point(656, 31)
        Me.nupMontTo.Maximum = New Decimal(New Integer() {12, 0, 0, 0})
        Me.nupMontTo.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nupMontTo.Name = "nupMontTo"
        Me.nupMontTo.Size = New System.Drawing.Size(45, 23)
        Me.nupMontTo.TabIndex = 12
        Me.nupMontTo.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.TableLayoutPanel2)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel3.Location = New System.Drawing.Point(3, 19)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(1243, 70)
        Me.Panel3.TabIndex = 0
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel2.ColumnCount = 5
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 152.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 29.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 158.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.cmdClose, 4, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.cmdSaveCSV, 2, 0)
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(3, 3)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 1
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(1237, 60)
        Me.TableLayoutPanel2.TabIndex = 0
        '
        'cmdClose
        '
        Me.cmdClose.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdClose.Location = New System.Drawing.Point(1082, 3)
        Me.cmdClose.Name = "cmdClose"
        Me.cmdClose.Size = New System.Drawing.Size(152, 54)
        Me.cmdClose.TabIndex = 1
        Me.cmdClose.Text = "閉じる"
        Me.cmdClose.UseVisualStyleBackColor = True
        '
        'cmdSaveCSV
        '
        Me.cmdSaveCSV.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdSaveCSV.Location = New System.Drawing.Point(901, 3)
        Me.cmdSaveCSV.Name = "cmdSaveCSV"
        Me.cmdSaveCSV.Size = New System.Drawing.Size(146, 54)
        Me.cmdSaveCSV.TabIndex = 0
        Me.cmdSaveCSV.Text = "CSV出力"
        Me.cmdSaveCSV.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox3.Controls.Add(Me.Panel3)
        Me.GroupBox3.Location = New System.Drawing.Point(8, 886)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(1249, 92)
        Me.GroupBox3.TabIndex = 4
        Me.GroupBox3.TabStop = False
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.optDIV2)
        Me.GroupBox4.Controls.Add(Me.optDIV1)
        Me.GroupBox4.Location = New System.Drawing.Point(8, 13)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(289, 60)
        Me.GroupBox4.TabIndex = 0
        Me.GroupBox4.TabStop = False
        '
        'optDIV2
        '
        Me.optDIV2.AutoSize = True
        Me.optDIV2.Location = New System.Drawing.Point(123, 23)
        Me.optDIV2.Name = "optDIV2"
        Me.optDIV2.Size = New System.Drawing.Size(138, 20)
        Me.optDIV2.TabIndex = 1
        Me.optDIV2.Text = "メニュー別集計"
        Me.optDIV2.UseVisualStyleBackColor = True
        '
        'optDIV1
        '
        Me.optDIV1.AutoSize = True
        Me.optDIV1.Checked = True
        Me.optDIV1.Location = New System.Drawing.Point(32, 23)
        Me.optDIV1.Name = "optDIV1"
        Me.optDIV1.Size = New System.Drawing.Size(74, 20)
        Me.optDIV1.TabIndex = 0
        Me.optDIV1.TabStop = True
        Me.optDIV1.Text = "職員別"
        Me.optDIV1.UseVisualStyleBackColor = True
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.optDayMon2)
        Me.GroupBox5.Controls.Add(Me.optDayMon1)
        Me.GroupBox5.Location = New System.Drawing.Point(319, 12)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(200, 61)
        Me.GroupBox5.TabIndex = 1
        Me.GroupBox5.TabStop = False
        '
        'optDayMon2
        '
        Me.optDayMon2.AutoSize = True
        Me.optDayMon2.Location = New System.Drawing.Point(106, 22)
        Me.optDayMon2.Name = "optDayMon2"
        Me.optDayMon2.Size = New System.Drawing.Size(58, 20)
        Me.optDayMon2.TabIndex = 1
        Me.optDayMon2.Text = "月別"
        Me.optDayMon2.UseVisualStyleBackColor = True
        '
        'optDayMon1
        '
        Me.optDayMon1.AutoSize = True
        Me.optDayMon1.Checked = True
        Me.optDayMon1.Location = New System.Drawing.Point(19, 22)
        Me.optDayMon1.Name = "optDayMon1"
        Me.optDayMon1.Size = New System.Drawing.Size(58, 20)
        Me.optDayMon1.TabIndex = 0
        Me.optDayMon1.TabStop = True
        Me.optDayMon1.Text = "日別"
        Me.optDayMon1.UseVisualStyleBackColor = True
        '
        'lblRowCount
        '
        Me.lblRowCount.Font = New System.Drawing.Font("HG丸ｺﾞｼｯｸM-PRO", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblRowCount.Location = New System.Drawing.Point(707, 28)
        Me.lblRowCount.Name = "lblRowCount"
        Me.lblRowCount.Size = New System.Drawing.Size(75, 26)
        Me.lblRowCount.TabIndex = 15
        Me.lblRowCount.Text = "9999"
        Me.lblRowCount.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label4
        '
        Me.Label4.Font = New System.Drawing.Font("HG丸ｺﾞｼｯｸM-PRO", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label4.Location = New System.Drawing.Point(788, 28)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(52, 26)
        Me.Label4.TabIndex = 16
        Me.Label4.Text = "件"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmAggregate
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(11.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1264, 985)
        Me.Controls.Add(Me.GroupBox5)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.GroupBox3)
        Me.Font = New System.Drawing.Font("HG丸ｺﾞｼｯｸM-PRO", 12.0!)
        Me.Margin = New System.Windows.Forms.Padding(6, 4, 6, 4)
        Me.Name = "frmAggregate"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "集計画面"
        Me.GroupBox2.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        CType(Me.dgvList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        CType(Me.nupYear, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nupYearFrom, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nupMont, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nupMontFrom, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nupYearTo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nupMontTo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents Panel2 As Panel
    Friend WithEvents dgvList As DataGridView
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents Panel1 As Panel
    Friend WithEvents TableLayoutPanel1 As TableLayoutPanel
    Friend WithEvents Label1 As Label
    Friend WithEvents txtEmpCode As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents txtKanaName As TextBox
    Friend WithEvents cmdSearch As Button
    Friend WithEvents cmdClear As Button
    Friend WithEvents Panel3 As Panel
    Friend WithEvents TableLayoutPanel2 As TableLayoutPanel
    Friend WithEvents cmdClose As Button
    Friend WithEvents cmdSaveCSV As Button
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents nupYear As NumericUpDown
    Friend WithEvents nupMont As NumericUpDown
    Friend WithEvents GroupBox4 As GroupBox
    Friend WithEvents optDIV2 As RadioButton
    Friend WithEvents optDIV1 As RadioButton
    Friend WithEvents optPurchase2 As RadioButton
    Friend WithEvents optPurchase1 As RadioButton
    Friend WithEvents nupYearFrom As NumericUpDown
    Friend WithEvents nupMontFrom As NumericUpDown
    Friend WithEvents Label3 As Label
    Friend WithEvents nupYearTo As NumericUpDown
    Friend WithEvents nupMontTo As NumericUpDown
    Friend WithEvents sfdCsvFile As SaveFileDialog
    Friend WithEvents GroupBox5 As GroupBox
    Friend WithEvents optDayMon2 As RadioButton
    Friend WithEvents optDayMon1 As RadioButton
    Friend WithEvents COL_PURCHASE_DATE As DataGridViewTextBoxColumn
    Friend WithEvents COL_GROUP_CD As DataGridViewTextBoxColumn
    Friend WithEvents COL_SECTION_CD As DataGridViewTextBoxColumn
    Friend WithEvents COL_USER_ID As DataGridViewTextBoxColumn
    Friend WithEvents COL_USER_NAME As DataGridViewTextBoxColumn
    Friend WithEvents COL_USER_KANA As DataGridViewTextBoxColumn
    Friend WithEvents COL_MENU_A As DataGridViewTextBoxColumn
    Friend WithEvents COL_MENU_B As DataGridViewTextBoxColumn
    Friend WithEvents COL_MENU_C As DataGridViewTextBoxColumn
    Friend WithEvents COL_ID As DataGridViewTextBoxColumn
    Friend WithEvents COL_CID As DataGridViewTextBoxColumn
    Friend WithEvents COL_SUM As DataGridViewTextBoxColumn
    Friend WithEvents lblRowCount As Label
    Friend WithEvents Label4 As Label
End Class
