﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMenu
    Inherits System.Windows.Forms.Form

    'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows フォーム デザイナーで必要です。
    Private components As System.ComponentModel.IContainer

    'メモ: 以下のプロシージャは Windows フォーム デザイナーで必要です。
    'Windows フォーム デザイナーを使用して変更できます。  
    'コード エディターを使って変更しないでください。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.cmdBack = New System.Windows.Forms.Button()
        Me.cmdHistory = New System.Windows.Forms.Button()
        Me.cmdUserMainte = New System.Windows.Forms.Button()
        Me.cmdMenuMainte = New System.Windows.Forms.Button()
        Me.cmdAggregate = New System.Windows.Forms.Button()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.TableLayoutPanel2, 1, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.cmdHistory, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.cmdMenuMainte, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.cmdAggregate, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.cmdUserMainte, 1, 1)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 3
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 108.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(1130, 512)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 2
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.cmdBack, 1, 0)
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(569, 407)
        Me.TableLayoutPanel2.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 1
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(557, 100)
        Me.TableLayoutPanel2.TabIndex = 0
        '
        'cmdBack
        '
        Me.cmdBack.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdBack.Font = New System.Drawing.Font("HG丸ｺﾞｼｯｸM-PRO", 15.75!, System.Drawing.FontStyle.Bold)
        Me.cmdBack.Location = New System.Drawing.Point(282, 3)
        Me.cmdBack.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdBack.Name = "cmdBack"
        Me.cmdBack.Size = New System.Drawing.Size(271, 94)
        Me.cmdBack.TabIndex = 0
        Me.cmdBack.Text = "戻る"
        Me.cmdBack.UseVisualStyleBackColor = True
        '
        'cmdHistory
        '
        Me.cmdHistory.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdHistory.Font = New System.Drawing.Font("HG丸ｺﾞｼｯｸM-PRO", 15.75!, System.Drawing.FontStyle.Bold)
        Me.cmdHistory.Location = New System.Drawing.Point(4, 3)
        Me.cmdHistory.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdHistory.Name = "cmdHistory"
        Me.cmdHistory.Size = New System.Drawing.Size(557, 196)
        Me.cmdHistory.TabIndex = 1
        Me.cmdHistory.Text = "購入履歴"
        Me.cmdHistory.UseVisualStyleBackColor = True
        '
        'cmdUserMainte
        '
        Me.cmdUserMainte.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdUserMainte.Font = New System.Drawing.Font("HG丸ｺﾞｼｯｸM-PRO", 15.75!, System.Drawing.FontStyle.Bold)
        Me.cmdUserMainte.Location = New System.Drawing.Point(569, 205)
        Me.cmdUserMainte.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdUserMainte.Name = "cmdUserMainte"
        Me.cmdUserMainte.Size = New System.Drawing.Size(557, 196)
        Me.cmdUserMainte.TabIndex = 3
        Me.cmdUserMainte.Text = "ユーザメンテナンス"
        Me.cmdUserMainte.UseVisualStyleBackColor = True
        Me.cmdUserMainte.Visible = False
        '
        'cmdMenuMainte
        '
        Me.cmdMenuMainte.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdMenuMainte.Font = New System.Drawing.Font("HG丸ｺﾞｼｯｸM-PRO", 15.75!, System.Drawing.FontStyle.Bold)
        Me.cmdMenuMainte.Location = New System.Drawing.Point(4, 205)
        Me.cmdMenuMainte.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdMenuMainte.Name = "cmdMenuMainte"
        Me.cmdMenuMainte.Size = New System.Drawing.Size(557, 196)
        Me.cmdMenuMainte.TabIndex = 2
        Me.cmdMenuMainte.Text = "ランチメニューメンテナンス"
        Me.cmdMenuMainte.UseVisualStyleBackColor = True
        '
        'cmdAggregate
        '
        Me.cmdAggregate.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdAggregate.Font = New System.Drawing.Font("HG丸ｺﾞｼｯｸM-PRO", 15.75!, System.Drawing.FontStyle.Bold)
        Me.cmdAggregate.Location = New System.Drawing.Point(568, 3)
        Me.cmdAggregate.Name = "cmdAggregate"
        Me.cmdAggregate.Size = New System.Drawing.Size(558, 196)
        Me.cmdAggregate.TabIndex = 4
        Me.cmdAggregate.Text = "集計表"
        Me.cmdAggregate.UseVisualStyleBackColor = True
        '
        'frmMenu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(11.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.ClientSize = New System.Drawing.Size(1130, 512)
        Me.ControlBox = False
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Font = New System.Drawing.Font("HG丸ｺﾞｼｯｸM-PRO", 12.0!)
        Me.Margin = New System.Windows.Forms.Padding(5, 4, 5, 4)
        Me.Name = "frmMenu"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "食券管理システム-メニュー"
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents TableLayoutPanel1 As TableLayoutPanel
    Friend WithEvents TableLayoutPanel2 As TableLayoutPanel
    Friend WithEvents cmdBack As Button
    Friend WithEvents cmdHistory As Button
    Friend WithEvents cmdMenuMainte As Button
    Friend WithEvents cmdUserMainte As Button
    Friend WithEvents cmdAggregate As Button
End Class
